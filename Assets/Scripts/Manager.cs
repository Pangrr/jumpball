﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : Singleton<Manager>
{
    [System.Serializable]
    public class BallParameters
    {

        [Tooltip("Second")]
        [SerializeField] [Range(1, 60)] private int time = 5;
        public int t
        {
            get
            {
                return time;
            }
            set
            {
                if (value < 1)
                {
                    Debug.LogError("The value cannot be less than 1");
                }
                else
                {
                    time = value;
                }
            }
        }

        [Tooltip("Ball height from the ground")]
        [SerializeField] [Range(1, 8)] private int height = 4;
        public int h
        {
            get
            {
                return height;
            }
            set
            {
                if (value < 1)
                {
                    Debug.LogError("The value cannot be less than 1");
                }
                else
                {
                    height = value;
                }    
            }
        }
    }

    [SerializeField] private bool _pauseInTimeStop = false;
    public bool pauseInTimeStop => _pauseInTimeStop;

    public BallParameters ballParameters;
}
