﻿using UnityEngine;
using UnityEngine.UI;

public class BallManager : MonoBehaviour
{

    [SerializeField] private Text touchGroundText;
    [SerializeField] private GameObject replay;
    [SerializeField] private Transform groundPos;
    [SerializeField] private BoxCollider2D groundCollider;

    private int touchGround = 0;
    private bool stopCount = true;
    private float timer = 0;


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ground" && !stopCount)
        {
            touchGround++;
            touchGroundText.text = "Score: " + touchGround.ToString();
        }

    }

    public void EnterStart()
    {
        gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        touchGround = 0;
        touchGroundText.text = "Score: 0";
        timer = Time.time + Manager.Instance.ballParameters.t;

        //ground + height
        transform.position = new Vector2(transform.position.x,
            (groundPos.position.y + groundCollider.bounds.size.y) + Manager.Instance.ballParameters.h);

        stopCount = false;

        if (Time.timeScale == 0)
        {
            Time.timeScale = 1;
        }
    }

    void Start()
    {

    }

    void Update()
    {

        if (Time.time > timer && !stopCount)
        {
            stopCount = true;
            replay.SetActive(true);
            Server.Instance.scores.Add(touchGround);
            if (Manager.Instance.pauseInTimeStop)
            {
                Time.timeScale = 0;
            }

            Debug.Log("stop");
        }
    }


}
