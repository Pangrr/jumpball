﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestAnotherDev : MonoBehaviour
{
    
    void Start()
    {
        ChangeParametersBall();
    }

    public void ChangeParametersBall()
    {
        Manager.Instance.ballParameters.h = 4;
        Manager.Instance.ballParameters.t = 2;
    }
   
}
